---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第03章：运算符、表达式和语句

<!-- slide -->
# 3.1 运算符与表达式

1. Java语言中的绝大多数运算符和C语言相同，基本语句，如条件分支语句、循环语句等也和C语言类似
1. 二目逻辑运算符跟C语言一样，也具有短路规则:book:P36
1. 运算符的结合性决定了具有相同级别运算符的先后顺序
1. Java提供逻辑右移（无符号右移）运算符`>>>`（高位补`0`）

<!-- slide -->
# 位移运算符

```java{.line-numbers}
byte a=~127;
System.out.println(a);//-128
a=(byte)(a>>>2);
System.out.println(a);//-32
```

:warning: 不足32位的数据类型（`byte`,`char`,`short`）进行位移运算时会先转成`int`

<!-- slide -->
# 3.1.8：`instanceof`运算符

二目运算符，左元是一个对象，右元是一个类

```java{.line-numbers}
String s=new String(“abc”);
if(s instanceof  String){
    System.out.println(“s is a string”);
}
```

<!-- slide -->
# 3.2：语句概述:book:P38

1. 方法（`method`）调用语句
1. 表达式语句
1. 复合语句（语句块）
1. 空语句
1. 控制语句
1. `package`语句和`import`语句（程序结构语句）

<!-- slide -->
# 3.3：`if`条件分支语句:book:P38

```java{.line-numbers}
if(<boolean_expression>)
if(<boolean_expression>) - else
if(<boolean_expression>) - else if(<boolean_expression>) - ... - else
```

:warning: `boolean_expression`必须为`boolean`型，不能用`0`代表`false`、用`非0`代表`true`

<!-- slide -->
# 3.4：`switch`开关语句:book:P41

1. `byte`, `char`, `short`, `int`
1. JDK5.0
    + `Byte`,`Character`, `Short`, `Integer`
    + `enum`
1. JDK7.0
    1. `String`

**:warning:Java不能为`long`和`boolean`，C++可以为`long`和`bool`**

<!-- slide -->
# :warning:`default`可放置于任意位置（不推荐）:book:P42

```java{.line-numbers}
switch(number) {
    case 27 :  System.out.println(number+"是二等奖");
        break;
    default:    System.out.println(number+"未中奖");
        break;
    case 316 :
    case 59 :   System.out.println(number+"是一等奖");
        break;
}
```

:point_right:分别输入`58`和`59`出现什么输出？`default`语句去掉`break`时呢？

<!-- slide -->
# 3.5.1：`for`循环语句:book:P43

<!-- slide -->
# 3.5.2：`while`循环:book:P44

<!-- slide -->
# 3.5.3：`do-while`循环:book:P44

<!-- slide -->
# 3.6：`break`和`continue`语句:book:P45

<!-- slide -->
# 3.7：for语句与数组（`foreach`）:book:P46

```java{.line-numbers}
for(<element_type> <var_name>:<array_name>){

}
```

:warning: `var_name`不可以使用已声明的变量

<!-- slide -->
# 3.8：应用举例:book:P47

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
