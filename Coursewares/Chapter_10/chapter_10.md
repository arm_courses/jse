---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第10章：输入、输出流

<!-- slide -->
# 本章主要内容

1. File类
1. BIO, NIO与AIO体系
1. 文件字节和文件字符输入输出流
1. 节点流与处理流、随机流
1. 序列化与对象克隆
1. Scanner解析文件
1. NIO文件锁

<!-- slide -->
# 本章重点难点

1. 重点：通过文件流读写数据，各种数据流的区别，掌握流的连接思想
1. 难点：流的连接思想及各种流的使用

<!-- slide -->
# 概述:book:P281

1. 将输入输出过程抽象成“流”，流是有顺序的、有起点和终点的一组基元的集合
1. 以CPU（或主机）为中心结点定义是输入还是输出

<!-- slide -->
# 10.1：File:book:P281

1. **`File`** 主要用于获取关于文件的一些属性（元数据或上下文）
1. **`File`** 不涉及对文件的读写操作
1. 目录（文件夹）也是 **`File`**

```java{.line-numbers}
package java.io;

File(String filename);
File(String directoryPath,String filename);
File(File f, String filename);
```

<!-- slide -->
# 10.1.1：`File`

```java{.line-numbers}
//java.io.File
public String getName();
public boolean canRead();
public boolean canWrite();
public boolean exits();
public long length();
public String getAbsolutePath();
public String getParent();
public boolean isFile();
public boolean isDirectroy();
public boolean isHidden();
public long lastModified();
```

<!-- slide -->
# Example1001:book:282

<!-- slide -->
# `FileDescriptor`

```java{.line-numbers}
/**Instances of the file descriptor class serve as an opaque handle to \
    the underlying machine-specific structure representing an open file, \
    an open socket, or another source or sink of bytes. \
    The main practical use for a file descriptor is to \
    create a FileInputStream or FileOutputStream to contain it.
Applications should not create their own file descriptors.*/
public final class FileDescriptor extends Object;

/**Creates a FileInputStream by opening a connection to an actual file, \
    the file named by the File object file in the file system. \
    A new FileDescriptor object is created to represent this file connection.*/
public FileInputStream(File file) throws FileNotFoundException;
```

<!-- slide -->
# 10.1.2：目录

```java{.line-numbers}
//java.io.File
public boolean mkdir();

public String[] list();
public File [] listFiles();
public String[] list(FilenameFilter obj);
public File [] listFiles(FilenameFilter obj);
public boolean accept(File dir,String name);
```

<!-- slide -->
# Example1002:book:P283

<!-- slide -->
# 10.1.3：文件的创建与删除

```java{.line-numbers}
public boolean createNewFile();
public boolean delete();
```

<!-- slide -->
# 10.1.4：运行可执行文件

```java{.line-numbers}
//path
java.lang.Runtime.getRuntime().exec(String command);
```

<!-- slide -->
# Example1003:book:P284

<!-- slide -->
# java.io体系

1. 按流向方向分类： 输入流与输出流
1. 按处理角色分类： 节点流与处理流
    1. 节点流Node Stream： 指向一个特定的地方（节点）读写流
    1. 处理流Filter Stream： 基于Adapter Design Pattern，有时也称为过滤流或链接流
        1. **对已存在的流的封装：** 构造方法接受其他的流对象
        1. 一个流对象可经多次包装
1. 按操作基元分类： 字节流与字符流
    1. 字节流Byte Stream： 以字节为操作基元
    1. 字符流Character Stream： 以字符为操作基元（与字符集有关）

## :exclamation:注意与C的对比

<!-- slide -->
# java.io体系（续）

1. 4个抽象流基类：**`InputStream`**, **`Reader`**, **`OutputStream`**, **`Writer`**
1. 4个抽象过滤流基类： **`FilterInputStream`**, **`FilterOutputStream`**, **`FilterReader`**, **`FilterWriter`**
1. 独立的类： **`RandomAccessFile`**

:point_right:详细的`java.io`类的分类请见本文件夹下的[java_bio_system.pdf](./java_bio_system.pdf)

```java{.line-numbers}
//java.lang.System
public static final InputStream in;
public static final PrintStream out;
public static final PrintStream err;
```

<!-- slide -->
# `java.nio`

1. JDK1.4引入，N代表Non-blocking和New
1. IO与NIO的区别
    1. IO面向 **byte级** ，NIO面向 **block级** (OS层缓冲区)
    1. IO流是阻塞的，NIO流是不阻塞的
    1. IO没有选择器，NIO有选择器
1. NIO核心组件：channels, buffers, selectors
1. NIO诞生原因：提高IO效率、匹配OS的IO模型（DMA）

## :exclamation:JDK1.4中的BIO和NIO已进行集成整合（即`java.io`重写并与`java.nio`相互协作）

<!-- slide -->
# BIO vs NIO vs AIO

|BIO|NIO|AIO|
|--|--|--|
|Blocking IO|Non-blocking IO|Asynchronous IO|
|JDK1.0|JDK1.4|JDK1.7|
| **`java.io`** | **`java.nio`** | **`java.nio`** |

## [漫话解释Linux的五种IO模型](https://mp.weixin.qq.com/s?__biz=Mzg3MjA4MTExMw==&mid=2247484746&idx=1&sn=c0a7f9129d780786cabfcac0a8aa6bb7&source=41#wechat_redirect)

<!-- slide -->
# 10.2：文件字节输入流:book:P285

1. 使用输入流通常包括4个基本步骤：
    1. 设定输入流的源
    1. 创建指向源的输入流
    1. 让输入流读取源中的数据
    1. 关闭输入流

<!-- slide -->
# `InputStream`

```java{.line-numbers}

//three important methods
/**Reads the next byte of data from the input stream. \
    The value byte is returned as an int in the range 0 to 255.*/
public abstract int read() throws IOException;
public int read(byte[] b) throws IOException；
public int read(byte[] b, int off, int len) throws IOException;


public long skip(long n) throws IOException;
public void mark(int readlimit);
public void reset() throws IOException;
public boolean markSupported();

public void close() throws IOException;
```

<!-- slide -->
# `OutputStream`

```java{.line-numbers}

//three important methods
/**Writes the specified byte to this output stream.*/
public abstract void write(int b) throws IOException;
public void write(byte[] b) throws IOException;
public void write(byte[] b, int off, int len) throws IOException;

/**The flush method of OutputStream does nothing.*/
public void flush() throws IOException;
public void close() throws IOException;
```

<!-- slide -->
# :thinking::thinking::thinking:

1. 为什么读写单个byte时用的都是`int`型:question:
1. 为什么在Java中仍需要`close()`流进行资源释放:question:
1. `close()`的流如何重新打开:question:

<!-- slide -->
# `FileInputStream` && Example1004:book:P286

```java{.line-numbers}
public FileInputStream(File file) throws FileNotFoundException;
public FileInputStream(String name) throws FileNotFoundException;

public int read() throws IOException;
public int read(byte[] b) throws IOException;
public int read(byte[] b, int off, int len) throws IOException;

/**Closes this file input stream and releases any system resources \
    associated with the stream.
If this stream has an associated channel then the channel is closed as well.*/
public void close() throws IOException;
```

<!-- slide -->
# 10.3：文件字节输出流:book:P287

1. 使用输出流通常包括4个基本步骤：
    1. 设定输出流的目的地
    1. 创建指向目的地的输出流
    1. 让输出流把数据写入到目的地
    1. 关闭输出流

<!-- slide -->
# `FileOutputStream` && Example1005:book:P289

```java{.line-numbers}
public FileOutputStream(File file) throws FileNotFoundException;
public FileOutputStream(File file, boolean append) throws FileNotFoundException;
public FileOutputStream(String name) throws FileNotFoundException;
public FileOutputStream(String name, boolean append) throws FileNotFoundException;

/**Writes the specified byte to this file output stream. \
    Implements the write method of OutputStream.*/
public void write(int b) throws IOException;
public void write(byte[] b) throws IOException;
public void write(byte[] b, int off, int len) throws IOException;

/**Closes this file input stream and releases any system resources \
    associated with the stream.
If this stream has an associated channel then the channel is closed as well.*/
public void close() throws IOException;
```

<!-- slide -->
# 10.4：文件字符输入输出流:book:P289

<!-- slide -->
# `Reader`

```java{.line-numbers}
//three important methods
public int read() throws IOException;
public int read(char[] cbuf) throws IOException;
public abstract int read(char[] cbuf, int off, int len) throws IOException;
/**Skips characters.*/
public long skip(long n) throws IOException;
```

<!-- slide -->
# `Writer`

:warning:`write(int c)`写入字符

```java{.line-numbers}
//three important methods
public void write(int c) throws IOException;
public void write(char[] cbuf) throws IOException；
public abstract void write(char[] cbuf, int off, int len) throws IOException;
```

<!-- slide -->
# `FileReader` and `FileWriter`

@import "./00_Diagram/FileReader_and_FileWriter.puml"

<!-- slide -->
# Example1006:book:P290

## :exclamation:`out.flush()`此语句没有任意作用

<!-- slide -->
# charset

1. **`Reader`** 和 **`Writer`** 不提供charset功能
1. **`InputStreamReader`** 和 **`OutputStreamWriter`** 的charset在构造方法中设置，成员方法只提供 **`getEncoding`**
1. **`FileReader`** 和 **`FileWriter`** 的构造方法中没有charset参数

```java{.line-numbers}
//java.io.FileReader
Convenience class for reading character files. \
    The constructors of this class assume that the default character encoding \
    and the default byte-buffer size are appropriate. \
    To specify these values yourself, \
    construct an InputStreamReader on a FileInputStream.
```

<!-- slide -->
# `InputStreamReader`

```java{.line-numbers}
/**Creates an InputStreamReader that uses the default charset.*/
public InputStreamReader(InputStream in);
/**Creates an InputStreamReader that uses the named charset.*/
public InputStreamReader(InputStream in, String charsetName) \
    throws UnsupportedEncodingException;

new InputStreamReader(new FileInputStream("fileName"), "charsetName");
```

<!-- slide -->
# :thinking:什么时候用字节流:question:什么时候用字符流:question:

<!-- slide -->
# 10.5：缓冲流:book:P290

```java{.line-numbers}
/**The flush method of OutputStream does nothing.*/
public void OutputStream.flush() throws IOException;
```

<!-- slide -->
# `BufferedWriter`

```java{.line-numbers}
/**Creates a buffered character-output stream that \
    uses a default-sized output buffer.*/
public BufferedWriter(Writer out);
/**Creates a new buffered character-output stream that \
    uses an output buffer of the given size.*/
public BufferedWriter(Writer out, int sz);
/**Writes a portion of a String.*/
public void write(String s, int off, int len) throws IOException;
/**Writes a line separator. The line separator string \
    is defined by the system property line.separator, \
    and is not necessarily a single newline ('\n') character.*/
public void newLine() throws IOException;
```

<!-- slide -->
# `BufferedReader`

```java{.line-numbers}
/**Creates a buffering character-input stream that \
    uses a default-sized input buffer.*/
public BufferedReader(Reader in);
/**Creates a buffering character-input stream that \
    uses an input buffer of the specified size.*/
public BufferedReader(Reader in, int sz);
/**Reads a line of text. A line is considered to be terminated \
    by any one of a line feed ('\n'), a carriage return ('\r'), \
    or a carriage return followed immediately by a linefeed.*/
public String readLine() throws IOException;
```

<!-- slide -->
# Example1013:book:P292

:exclamation:**`out.close()`** 和 **`in.close()`** 没有必要

```java{.line-numbers}
BufferedReader writer=new BufferedReader(new FileWriter(new File("xxx")));

/**Closes the stream, flushing it first. Once the stream has been closed, \
    further write() or flush() invocations will cause an IOException to be thrown. \
    Closing a previously closed stream has no effect.*/
public abstract void close() throws IOException;
```

<!-- slide -->
# 10.6：随机访问流:book:P292

1. **`RandomAccessFile`** 即可以输入也可以输出
1. **`RandomAccessFile`** 可以随机定位到特定偏移位置

```java{.line-numbers}
public RandomAccessFile(File file, String mode) throws FileNotFoundException;
public RandomAccessFile(String name, String mode) throws FileNotFoundException;
```

<!-- slide -->
# Modes

1. **`"r"`:** Open for reading only. Invoking any of the write methods of the resulting object will cause an IOException to be thrown.
1. **`"rw"`:** Open for reading and writing. If the file does not already exist then an attempt will be made to create it.
1. **`"rws"`:** Open for reading and writing, as with "rw", and also require that every update to the file's content or metadata be written synchronously to the underlying storage device.
1. **`"rwd"`:** Open for reading and writing, as with "rw", and also require that every update to the file's content be written synchronously to the underlying storage device.

## :thinking:为什么没有`"b"`这个模式:question:

<!-- slide -->
# Methods

```java{.line-numbers}
public long getFilePointer() throws IOException;
public void seek(long pos) throws IOException;
public final String readUTF() throws IOException;

/**Each byte is converted into a character by taking the byte's value for the lower \
    eight bits of the character and setting the high eight bits of the character to \
    zero. This method does not, therefore, support the full Unicode character set.*/
public final String readLine() throws IOException;
```

<!-- slide -->
# Example1008:book:P293 && Example1009:book:P295

<!-- slide -->
# 10.7：数组流:book:P295

<!-- slide -->
# 字节数组输入流`ByteArrayInputStream`

```java{.line-numbers}
public ByteArrayInputStream(byte[] buf);
public ByteArrayInputStream(byte[] buf, int offset, int length);

public int read() throws IOException;
public int read(byte[] b) throws IOException;
public int read(byte[] b, int off, int len) throws IOException;
```

<!-- slide -->
# 字节数组输出流`ByteArrayOutputStream`

```java{.line-numbers}
public ByteArrayOutputStream();
public ByteArrayOutputStream(int size);

public void write(int b) throws IOException;
public void write(byte[] b, int off, int len) throws IOException;
public byte[] toByteArray() throws IOException;
```

<!-- slide -->
# 字符数组输入流`CharArrayReader`

```java{.line-numbers}
public CharArrayReader(char[] buf);
public CharArrayReader(char[] buf, int offset, int length);

public int read() throws IOException;
public int read(char[] cbuf) throws IOException;
public int read(char[] b, int off, int len) throws IOException;

public long skip(long n) throws IOException;
```

<!-- slide -->
# Example1010:book:P298

<!-- slide -->
# 字符数组输出流`CharArrayWriter`

<!-- slide -->
# 10.8：数据流`DataInputStream`和`DataOutputStream`:book:P297

<!-- slide -->
# 数据输入流

```java{.line-numbers}
public DataInputStream(InputStream in);
public final XXX readXXX();

public DataOutputStream(OutputStream out);
public final void writeXXX(XXX);
```

<!-- slide -->
# Example1011:book:P297 && Example1012:book:P298

<!-- slide -->
# 10.9：对象流:book:P299

```java{.line-numbers}
public ObjectInputStream(InputStream in) throws IOException
public ObjectOutputStream(OutputStream out) throws IOException
```

<!-- slide -->
# `java.io.Serializable`

```java{.line-numbers}

/**Classes that require special handling during the \
    serialization and deserialization process must implement \
    special methods with these exact signatures
The serialization interface has no methods or fields and serves \
    only to identify the semantics of being serializable.*/
public interface Serializable;

//ObjectOutputStream
private void writeObject(java.io.ObjectOutputStream out) \
    throws IOException;
//ObjectInputStream
private void readObject(java.io.ObjectInputStream in) \
    throws IOException, ClassNotFoundException;
```

<!-- slide -->
# Example1013:book:P300

<!-- slide -->
# 10.10：序列化与对象克隆:book:P301 && Example1014:book:P302

1. clone： 复制对象本身，而不是对象引用（对象地址）
1. **`Object.clone()`** 方式克隆对象
1. **`ObjectInputStream`** 和 **`ObjectOutputStream`** 方式克隆对象

:exclamation: shallow clone vs deep clone :exclamation:

<!-- slide -->
# `java.lang.Cloneable`

```java{.line-numbers}
/**A class implements the Cloneable interface to indicate to \
    the Object.clone() method that it is legal for that method to \
    make a field-for-field copy of instances of that class.
Invoking Object's clone method on an instance that \
    does not implement the Cloneable interface results in \
    the exception CloneNotSupportedException being thrown.
By convention, classes that implement this interface should override \
    Object.clone (which is protected) with a public method. \
    See Object.clone() for details on overriding this method.
Note that this interface does not contain the clone method. \
    Therefore, it is not possible to clone an object merely by virtue of the fact that \
    it implements this interface. Even if the clone method is invoked reflectively, \
    there is no guarantee that it will succeed.*/
public interface Cloneable;
```

<!-- slide -->
# 10.11：使用Scanner解析文件:book:P303

```java{.line-numbers}
public Scanner(File source) throws FileNotFoundException;
public Scanner(File source, String charsetName) throws FileNotFoundException;
public Scanner(String source); public Scanner(InputStream source);
public Scanner(InputStream source, String charsetName);
```

<!-- slide -->
# Example1015:book:P304 && Example1016:book:P305

<!-- slide -->
# 10.12 文件对话框`JFileChooser`:book:P306

```java{.line-numbers}
public JFileChooser();
public JFileChooser(String currentDirectoryPath);
public JFileChooser(File currentDirectory);

public int showSaveDialog(Component parent) throws HeadlessException;
public int showOpenDialog(Component parent) throws HeadlessException;

the return state of the file chooser on popdown:
JFileChooser.CANCEL_OPTION
JFileChooser.APPROVE_OPTION
JFileChooser.ERROR_OPTION if an error occurs or the dialog is dismissed
```

<!-- slide -->
# Example1017:book:P306

<!-- slide -->
# 10.13 带进度条的输入流:book:P308

```java{.line-numbers}
public ProgressMonitorInputStream(Component parentComponent,
                                  Object message, InputStream in);
```

<!-- slide -->
# Example1018:book:P308

<!-- slide -->
# 10.14   文件锁:book:P309

```java{.line-numbers}
//java.nio.channels.FileLock
//java.nio.channels.FileChannel
RandomAccessFile input = new RandomAccessFile("Example.java","rw");
FileChannel channel = input.getChannel();
FileLock lock = channel.tryLock();
lock.release();

public FileChannel java.io.FileInputStream.getChannel();
public FileChannel java.io.FileOutputStream.getChannel();
```

<!-- slide -->
# 10.15：应用举例:book:P311

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
