---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第7章：内部类与异常类

<!-- slide -->
# 本章主要内容

1. 内部类和匿名类
1. 异常和断言

<!-- slide -->
# 本章重点和难点

1. 重点：内部类和异常类的理解
1. 难点：异常类的使用
    1. 什么时候哪个方法应该抛弃异常？
    1. 什么时候哪个方法应该处理异常？
    1. 应该使用哪种类型的异常？

<!-- slide -->
# 7.1：内部类Inner Class/Nested Class:book:P162

1. **内部类Inner Class/成员类Member Class：** 也称为 **Nested Class**
    1. **实例内部类Non-Static Inner Class/实例成员类Non-Static Member Class：** 在类中定义的非静态类
    1. **静态内部类Static Inner Class/静态成员类Static Memeber Class`：** 在类中定义的静态类
1. **外嵌类Outer Class：** 包含内部类或静态内部类的那个类

<!-- slide -->
# 内部类的使用规则

1. **实例内部类定义规则：** **实例内部类** 中 **不能定义类变量和类方法**
1. **内部类仅供外嵌类使用：** 除外嵌类外的其他类不能使用内部类声明对象:question::x:
1. **内部类是外嵌类的成员：** 声明内部类与声明方法或变量一样，作为是外嵌类的成员
1. **内部类对象作为外嵌类的成员变量：** 外嵌类可以声明内部类对象作为成员变量
1. **内部类`class`文件名：** 外嵌类和内部类在编译时分别生成对应的 **`class`** 文件，其中内部类的 **`class`** 文件名格式为 **`<outer_classname>$<inner_classname>`**
1. **内部类访问外嵌类的成员：** 实例内部类可以访问外嵌类的成员变量和成员方法，包含 **`private`** 成员，静态内部类不可访问外嵌类的非静态成员

<!-- slide -->
```java{.line-numbers}
public class DemoNested {
    public class ANested {
        public void sayHello() {
            System.out.println("Hello...");
        }
    }
    public static class AStaticNested {
        public void sayStaticHello(){
            System.out.println("Static Hello...");
        }
    }
    public static void main(String[] args) {
        new AnotherOne().accessANested();
    }
}
class AnotherOne {
    public void accessANested() {
        DemoNested demoNested = new DemoNested();
        DemoNested.ANested aNested = demoNested.new ANested();
        DemoNested.AStaticNested aStaticNested=new DemoNested.AStaticNested();
        aNested.sayHello();
        aStaticNested.sayStaticHello();
    }
}
```

<!-- slide -->
# 外部访问内部类

1. 引用变量： **`<OuterClassName>.<InnerClassName>`**
1. 创建对象：
    1. 实例内部类： **`<outerObject>.new InnerClassName()`**
    1. 静态内部类： **`new <OuterClassName>.<InnerClassName>`**

<!-- slide -->
```java{.line-numbers}
public class RedCowForm {
        static String formName;
        RedCow cow;//non-static inner class object
        FormToolkit formToolkit;//static inner class object
        RedCowForm() {  }
        RedCowForm(String s) {...}
        public void showCowMess() {
            cow.speak();
        }
        class RedCow {//inner class
            //cannot declare static members in inner class
            String cowName = "红牛";
            int height,weight,price;
            RedCow(int h,int w,int p){...}
            void speak() {//access formName in outer class
                System.out.println("偶是"+cowName+",身高:"+height+
                    "cm 体重:"+weight+"kg,生活在"+formName);
            }
        }
        public static class FormToolkit{
            static String toolkitName = "Nice Toolkit";
        }
}
```

<!-- slide -->
# 7.2：匿名类Anonymous class:book:P163

1. 匿名类是一种内部类
1. 匿名类没有自己的标识符，因此：
    1. 是只使用一次的类
    1. 必须继承一个类或实现一个接口
    1. 没有有名的构造方法，但可以有匿名的构造方法
1. 匿名类 **不能定义`static`成员**

<!-- slide -->
# 基于类的匿名类

## 基于某个类的某个构造方法，并定义类体，该匿名类是该类的子类

```java{.line-numbers}
new <classname>(...){
    //member define here...
    {//Anonymous constructor,will call super class's constructor first
    
    }
};
```

## :point_right: See Code in EG0702

<!-- slide -->
# 基于接口的匿名类

## 基于某个接口，并定义类体，该匿名类实现了该接口

```java{.line-numbers}
new <interfacename>(){
    //member define here...
    {//Anonymous constructor

    }
}
```

<!-- slide -->
# Java的4种内部类

1. 类中类：
    1. 实例内部类： 定义在类中的非静态内部类
    1. 静态内部类： 定义在类中的静态内部类
1. 方法中类： **不能用`static`修饰**
    1. 局部内部类： 定义在方法内的内部类
    1. 匿名内部类： 定义在方法内的语句中的没有名字的局部内部类

## :question: 内部类用在什么场景下

<!-- slide -->
# 7.3：异常类:book:P166

```java{.line-numbers}
public class Error extends Throwable

public class Exception extends Throwable

public class RuntimeException extends Exception
```

<!-- slide -->
# Error vs Exception

1. **错误Error：** 一般指与JVM相关的问题，如系统崩溃、虚拟机错误、内存空间不足、方法调用栈溢等，一般仅靠程序本身无法恢复和预防，遇到这样的错误，建议让程序终止，如 **`OutOfMemoryError`** 等
1. **异常Exception：** 一般指程序运行时可能出现的一些意外情况（可预测、可恢复的问题），异常处理可能改变程序的控制流程

<!-- slide -->
# Unchecked Exception vs Checked Exception

1. **运行时异常`RuntimeException`/非检查异常`Unchecked Exception`：** 可以捕捉，也可以不捕捉，一般由程序的逻辑错误引起的，应从程序编码中避免此类异常，如 **`NullPointerException`** 、 **`ArrayIndexOutOfBoundsException`** 等
1. **非运行时异常/检查异常`Checked Exception`/业务异常？：** 必须捕捉处理或声明异常（当调用声明会抛出此类异常的方法时）

<!-- slide -->
# `Exception`

```java{.line-numbers}
public String getMessage();
public void printStackTrace();
public String toString();
```

<!-- slide -->
# 7.3.1：try-catch-finally语句

1. `try{}`： 放置将可能出现的异常操作
1. `catch(...){}`： 放置捕捉对应异常后的处理操作
1. `finally{}`： 放置无论是否出现异常均要被处理的操作（除非在`try`或`catch`中执行`System.exit(0)`）

```java{.line-numbers}
try{}
catch(ExceptionSubClass e){}
finally{}
```

<!-- slide -->
# 7.3.2：声明异常和抛出异常

1. **声明异常`throws`：** 方法不捕捉它可能产生的异常，由调用它的方法来处理（或进一步沿着调用层次向上传递），叫声明异常
1. **抛出异常`throw`：** 抛出异常

```java{.line-numbers}
void compute(int x) throws  ArithmeticException, IOException{
    otherMethod();//otherMethod() may throw ArithmeticException
    throw new IOException("Throw a new IOException");
}

void otherMethod() throws ArithmeticException{}
```

<!-- slide -->
# 7.3.2：自定义异常类

1. 定义检查异常： 继承`Exception`
1. 定义运行时异常： 继承`RuntimeException`

```java{.line-numbers}
public class BankException extends Exception{
    String message;
    public BankException(int m,int n) {
        message="入账资金"+m+"是负数或支出"+n+"是正数，不符合系统要求.";
    }
    public String warnMess() {
        return message;
    }
}
```

<!-- slide -->
# 用异常还是用返回状态:question:

1. 处理异常比判断状态代价大
1. 异常的结构比判断状态清晰

## See ==> [优雅地使用异常](http://lanlingzi.cn/post/technical/2019/0615_execption/)

<!-- slide -->
# 7.4：断言Assertion:book:P169

1. 断言语句用于调试代码阶段，断言失败时产生`AssertionError`
1. `C/C++`与`Java`断言的区别：
    1. `C/C++`的断言是库函数中的宏，`Java`是关键字
    1. `C/C++`在编译期可关闭断言，`Java`在运行关闭断言
1. `Java`运行时默认关闭断言，通过 **`-ea`（Enable Assertions）** 开启断言

```java{.line-numbers}
public class AssertionError extends Error

assert booleanExpression;
assert booleanExpression:messageException;
```

<!-- slide -->
# 总结

1. Four types of Inner Class
1. Exception
    1. Difference between Exception and Error, between RuntimeException and Checked Exception
    1. `try-catch-finally`, `throws` and `throw`
1. Assertion

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
