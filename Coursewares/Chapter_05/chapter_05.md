---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第05章：子类与继承

<!-- slide -->
# 本章主要内容

1. 子类与父类、子类的继承性、子类成员变量的隐藏和方法重写
1. super关键字
1. final关键字
1. 对象的上转型
1. 继承与多态
1. abstract类与abstract方法
1. 面向抽象编程、开-闭原则

<!-- slide -->
# 本章重点和难点

1. 重点：继承、上转型和多态
1. 难点：上转型和多态的理解和运用、设计模式

<!-- slide -->
# 5.1：子类与父类

1. 继承是一种由已有的类创建新类的机制，由继承而得到的类称为子类，被继承的类称为父类（直接超类），子类继承父类的数据和行为，并根据需要增加新的数据和行为
    1. 先创建一个共有属性的一般类
    1. 创建具有特殊属性的新类
1. Java不支持 **多重继承** ，即，Java的子类只能有一个父类

## 子类与父类的关系是“is-a”关系

:exclamation::point_right:“自上而下”编码，“自下而上”抽象

<!-- slide -->
# 5.1.1：子类

使用关键字 **`extends`** 来定义继承的父类

```java{.line-numbers}
class <subclass_name> extends <superclass_name>{
    ...
}
```

<!-- slide -->
# 5.1.2：Java类的树形结构

## Java的类{==按继承关系==}形成{==树形结构==}

1. 根节点是 **`java.lang.Object`**，即 **`Object`** 是所有类的超类
1. 除 **`Object`** 外，每个类有且仅有一个父类、零个或多个子类
1. 除 **`Object`** 外，类声明中没有使用 **`extends`** 时，默认直接继承自 **`Object`**的，声明时，**`class A`** 与 **`class A extends Object`** 是等价的

<!-- slide -->
# 5.2：子类的继承性

1. 子类的成员中一部分由子类自己定义，另一部分从超类继承
1. 继承的成员指子类 **可以直接访问** 的超类成员
1. 继承的成员的 **访问权限不变**

<!-- slide -->
# 子类的继承性

```java{.line-numbers}
class Father{
    float weight,height;
    String head;
    void speak(String s){
        System.out.println(s);
    }
}
class Son extends Father{
    String hand,foot;

    void speak(String s){
        super.speak();
        //can access Father's weight,height,head
    }
}
Son s=new Son();
```

<!-- slide -->
# 各访问权限的继承可访问性

||`public`|`protected`|default|`private`|
|--|:--:|:--:|:--:|:--:|
|in the same package|:white_check_mark:|:white_check_mark:|:white_check_mark:|:x:|
|in difference package|:white_check_mark:|:white_check_mark:|:x:|:x:

:warning::book:P115，继承体系之外的类，对 **`protected`** 是否具有访问权限取决于该类与定义 **`protected`** 的类是否在同一个 **`package`** 中（在同一个 **`package`** 中可访问，不在同一个 **`package`** 不可访问）

<!-- slide -->
# `protected`的访问

```java{.line-numbers}
package outer;
class OuterClass{
    protected int i;
}

class OtherClass{
    void testAccess(){
        InnerClass ic = new InnerClass();
        ic.i = 100;//will be OK
    }
}

package inner;
class InnerClass extends OuterClass{}
```

<!-- slide -->
# 5.2.3：继承关系（Generalization）的UML图

1. 终点端为空心三角形的实线
1. 起始端是子类，终点端是父类

```puml
@startuml
SuperClass <|-- SubClass : Inheritance(is-a)
@enduml
```

<!-- slide -->
# 5.3：子类与对象

<!-- slide -->
# 子类对未继承的超类成员的访问

子类对未继承的超类成员通过 **继承的成员方法** 进行 **简接访问**

```java{.line-numbers}
class FatherClass{
    private int i;
    protected void setI(int i){
        this.i = i;
    }
}

class SonClass extends FatherClass{
    public void accessPrivate(){
        setI(100);
    }
}
```

<!-- slide -->
# 超类成员变量可访问性的几种情况

1. 超类的内部数据 ==> 子类和外部均无法直接访问
1. 继承体系的数据 ==> 子类可直接访问，外部无法直接访问
1. 公开访问的数据 ==> 子类和外部均可直接访问

:point_right:无法直接访问的数据，通过超类公开的成员方法简接访问

<!-- slide -->
# 5.3.1：子类对象的生成过程

1. 分配内存
1. 调用构造方法
    1. 初始化超类数据： 子类构造方法的首语句调用父类的某个构造方法，如果子类构造方法未显式调用父类构造方法则调用无参的构造方法（循环直至到根类 **`Object`**）
    1. 初始化子类数据： 执行子类构造方法剩余语句

:point_right:无论通过什么路径调用构造方法，都是先初始化超类数据，再初始化子类数据

<!-- slide -->
# 子类对象的生成过程（续）

1. 如果父类没有无参的默认构造方法，则子类构造方法中 **必须显式调用** 父类的某个有参构造方法
1. 显式调用其他构造方法必须在构造方法首条语句 ==> 显式调用自身的构造方法与显式调用父类的构造方法之间2选1

```java{.line-numbers}
public class SupClass {//don't has default constructor
    public SupClass(int i){
    }
}
class SubClass extends SupClass{
    public SubClass(){
        super(0);
    }
    public SubClass(int i){
        this();//==> must first statement
        //super(i); //==> must first statement
    }
}
```

<!-- slide -->
# 5.3.2：关于`instanceof`运算符:book:P117

**`instanceof`** 是双目运算符，左元引用变量或对象，右元是类名，当左元 **所引用的对象** 是右元的 **类或其子类所创建的对象** 时，结果为 **`true`**，否则为 **`false`**

```java{.line-numbers}
FatherClass fc=new SonClass();
System.out.println(fc instanceof SonClass);//true
fc = new FatherClass();
System.out.println(fc instanceof SonClass);//false
```

## `instanceof`属于Java RTTI的一员

## RTTI（RunTime Type Information/RunTime Type Identification）

<!-- slide -->
# 5.4：成员变量的隐藏和方法重写:book:P117

<!-- slide -->
# 5.4.1：成员变量的隐藏（Variable Hiding）

1. 子类中声明的成员变量与从超类继承的成员变量重名时，从超类继承的成员变量被隐藏（不能直接访问）
1. 使用 **`super`** 显式访问被隐藏的超类成员变量

<!-- slide -->
# Variable Hiding

```java{.line-numbers}
class Grandfather{
    protected int i;
}
class Father extends Grandfather{
    protected int i;
    public void setI(int val){
        i = val;//access Father's i
        super.i = val;//access Grandfather's i
    }
}
class Son extends Father{
    protected int i;
    public void setI(int val){
        i = val;//access Son's i
        super.i = val;//access Father's i
}
```

<!-- slide -->
# 5.4.2：成员方法重写（Method Overriding）

1. 成员方法重写的定义： 子类定义的方法的 **方法名与形参列表** 相同并且 **返回类型相同或是父类方法返回类型的子类型**
1. 成员方法重写的目的：**定义子类的特殊行为**、**实现多态**
1. 重写方法的访问权限：不可以降低访问权限，可以提高访问权限

:warning:**方法名相同、形参列表不同** 时为 **重载Overload**

:warning:**仅返回类型不与父类相同或不是其子类型** 时为 **编译错误**

<!-- slide -->
# 成员变量隐藏 VS 成员方法重写

1. 成员变量的隐藏与数据类型、修饰关键字均无关，只要标识符同名时即发生隐藏
1. 成员变量的隐藏没有多态性，即对象向上转型时，访问的是对应类型的成员变量

<!-- slide -->
# 成员方法重载 VS 成员方法重写

1. 多态时机：Overload是编译时多态，Override是运行时多态
1. 发生情况：Overload在继承体系和类内均会发生，Override在继承体系发生
1. 语法要求：Overload要求不同，Override要求相同

<!-- slide -->
# 5.5：`super`:book:P122

1. 使用`super.XX`显式调用父类的成员变量或成员方法
1. 使用`super(XX)`显式调用父类的构造方法（子类不继承父类的构造方法）

<!-- slide -->
# 5.6：`final`:book:P125

1. 修饰 **`class`**： 不能被继承，即不能有子类
1. 修饰 **`variable`**： 只读变量，即不能修改变量的值或引用，需在定义的同时初始化（成员变量可在声明时初始化或构造方法中初始化）
1. 修饰 **`method`**： 不能被子类重写

<!-- slide -->
# `final`

```java{.line-numbers}
final class A{
    final int iVal = 100;
    final float fVal = 100.01f;
    final double dVal ;
    public A(){
        dVal = 100.02;//will be OK
        final boolean bLocalVal = false;
    }
    public final void fMethod(){
    }
}
```

<!-- slide -->
# 5.7：对象的向上转型:book:P126

1. 将子类对象赋值给父类引用变量时发生向上转型
1. 上转型引用变量的使用：
    1. 上转型引用变量不能访问子类定义的成员变量和成员方法
    1. 上转型访问的成员变量是上转型后类型定义的成员变量（没有多态）
    1. 上转型访问的成员方法是对象实际类型的成员方法（重写时访问重写后的方法）

:warning:“向上转型”是能力减少的过程，隐式转换；“向下转型”是能力变强的过程，显式转换（无法转换时抛出 **`java.lang.ClassCastException`** ）

<!-- slide -->
# 5.8：继承与多态

多态是指父类的某个方法被其子类重写时，可以各自产生自己的功能行为

:point_right:多态指调用引用变量 **引用的对象的行为**，而不是 **引用变量的类型的行为**

<!-- slide -->
# 5.9：`abstract`类和`abstract()`方法:book:P129

1. 用 **`abstract`** 修饰的类称为abstract类（抽象类），抽象类不能被实例化
1. 用 **`abstract`** 修饰的方法称为abstract方法（抽象方法），抽象方法不能被定义，也不能用 **`final`** 修饰

<!-- slide -->
# abstract类的特点

1. abstract类 **可以有、也可以没有** abstract方法，非abstract类不能有abstract方法
1. abstract类的子类必须实现所有的abstract方法后才能变成非abstract类

:point_right:有 **`abstract method`** 的一定是 **`abstract class`** ， **`abstract class`** 的方法不一定是 **`abstract method`**

<!-- slide -->
# :question:理解抽象类

1. 抽象类可以抽象出重要的行为标准，该行为标准用抽象方法来表示，即抽象类封装了子类必需要有的行为标准
1. 抽象类声明的对象可以成为其子类的对象的上转型对象，调用子类重写的方法，即体现子类根据抽象类里的行为标准给出的具体行为
1. 开发者可以把主要精力放在一个应用中需要那些行为标准（不用关心行为的细节），不仅节省时间，而且非常有利于设计出易维护、易扩展的程序。抽象类中的抽象方法，可以由子类去实现，即行为标准的实现由子类完成

<!-- slide -->
# 抽象类的用途

1. 表征现实中无法实例化的抽象概念： 动物
1. 复用公用方法： 实现可实现的公共方法，**`abstract`** 无法实现的方法
1. 实现模板算法： 非抽象方法调用多个抽象方法，子类重写抽象方法

<!-- slide -->
# 设计模式：模板方法Template Method

```java{.line-numbers}
public abstract class Action {
    public final void process(){//let it be final
        <do_something>//code reuse
        action01();//code should be redefine
        <do_something>//code reuse
        action02();//code should be redefine
        <do_something>//code reuse
    }
    protected abstract void action01();
    protected abstract void action02();
}
```

<!-- slide -->
# Template Method的目标

>Define **the skeleton of an algorithm** in an operation, **deferring some steps to subclasses**. Template Method lets subclasses **redefine certain steps of an algorithm without changing the algorithm's structure**.

1. 提高代码复用：将算法流程和相同的代码放在抽象类中，将不同的代码放入不同的子类中
1. 实现开闭原则：子类对具体实现扩展不同的行为，抽象类无需修改，符合“开闭原则”

<!-- slide -->
# 5.10：面向抽象编程

:warning:教材中的“面向抽象编程”与“面向接口编程”表达的设计思维基本一样

面向抽象编程，当设计某个类时，该类面对的不是具体的类、而是抽象类

面向抽象编程，是指面向抽象角色编程，而不面向具体类编程，好处是程序可以灵活地增加功能，良好的解耦性、可扩展性，符合“开-闭”准则

@import "./00_Diagrams/pillar_geometry.puml"

<!-- slide -->
# 5.11：开-闭原则:book:P134

## “开-闭原则”（Open-Closed Principle） ==>对扩展开放，对修改关闭

1. 考虑用户需求的变化，将应对用户变化的部分设计为对扩展开放
1. 设计的核心部分是经过精心考虑之后确定下来的基本结构，这部分应当是对修改关闭的，即不能因为用户的需求变化而再发生变化，因为这部分不是用来应对需求变化的

遵循“开-闭原则”的系统 ==> **基础架构稳定**、**易维护**、**功能易扩展**

<!-- slide -->
# 满足开-闭原则的框架

![f_5.14](./00_Images/f_5.14.png)

<!-- slide -->
# 5.12：应用举例

要求：用类封装手机的基本属性和功能，要求手机即可以使用任何公司提供的SIM卡

1. 问题的分析
1. 设计抽象类
1. 设计手机类

<!-- slide -->
# 总结

1. 继承是一种从已有类创建新类的机制，主要体现复用的设计思维
1. 子类继承父类的成员，子类可以对父类成员进行变量隐藏和方法重写
1. 上转型时成员变量不体现多态、成员方法体现多态
1. 多态是OOP的重要思想和工具，体现一种运行时动态的特性
1. 运用“开-闭”原则设计系统

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
