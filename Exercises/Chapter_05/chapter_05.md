# Chapter 05 Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编程题（参考例子13）](#编程题参考例子13)

<!-- /code_chunk_output -->

## 问答题

1. 子类可以有多个父类吗？ => 不可以
2. java.lang包中的Object类是所有其他类的祖先类吗？ => 是（`Object`中定义了对所有类都重要的方法，比如`toString()`、`equals()`等）
3. 如果子类和父类不在同一个包中，子类是否继承父类的友好成员？ => 不继承，`default`访问权限（教材称为“友好成员”不准确）也称为`package`访问权限，即仅在包内可访问
4. 子类怎样隐藏继承的成员变量？ => 定义同名变量（与类型无关，即只需同名即隐藏）
5. 子类重写方法的规则是怎样的？重写方法的目的是什么？ => **重写要求相同**，即方法名、形参列表均相同、返回值的类型相同或是父类的子类型
6. 父类的final方法可以被子类重写吗？ => 不可以
7. 什么类中可以有abstract方法？ => `abstract class`
8. 对象的上转型对象有怎样的特点？ => 上转型的可访问的属性和方法以上转型后的类型为准，其中属性访问的是转型后的类型定义的属性（即属性没有多态性），方法实际调用的是重写的方法（即方法具有多态性）
9. 一个类的各个子类是怎样体现多态的？ => 重写方法
10. 面向抽象编程的目的和核心是什么？ => 具体类实现方法的具体内容

:warning::exclamation:教材中的“面向抽象编程”和“面向接口编程”实际上是相同的模型

## 选择题

1. **下列哪个叙述是正确的？**
A. 子类继承父类的构造方法。
B. abstract类的子类必须是非abstract类。
C. 子类继承的方法只能操作子类继承和隐藏的成员变量。
D. 子类重写或新增的方法也能直接操作被子类隐藏的成员变量。

答：`C`
解：
A => 构造方法不能继承
B => abstract类的子类也可是abstract类
C => 子类继承的方法是由父类定义的，所以只能操作子类继承和隐藏的成员变量（即父类定义的变量）
D => 不能 **直接** 操作，可以用`super`关键字简接操作

2. **下列哪个叙述是正确的？**
A. final 类可以有子类。
B. abstract类中只可以有abstract方法。
C. abstract类中可以有非abstract方法，但该方法不可以用final修饰。
D. 不可以同时用final和abstract修饰同一个方法。
E. 允许使用static修饰abstract方法。

答：`D`
解：
A => `final`类不可以有子类
B => abstract类可以有非abstract方法
C => 略
D => `final`要求不可重写，`abstract`要求必须重写，相矛盾
E => `static`是类方法、不能被重写（可重载、可继承、可隐藏），`abstract`要求被重写，相矛盾

3. **下列程序中注释的哪两个代码（A，B，C，D）是错误的（无法通过编译）？**

```java{.line-numbers}
class Father {
   private int money =12;
   float height;
   int seeMoney(){
      return money ;           //A
   }
}
class Son extends Father {
   int height;
   int lookMoney() {
      int m = seeMoney();      //B
      return m;
   }
}
class E {
   public static void main(String args[]) {
      Son erzi = new Son();
      erzi.money = 300;       //C
      erzi.height = 1.78F;      //D
   }
}
```

答：`CD`
解：
C => `money`是`private`
D => `erzi.height`是`int`型（`Son`类定义），将`float`赋值给`int`时需要强制类型转换

4. **假设C是B的子类，B是A的子类，cat是C类的一个对象，bird是B类的一个对象，下列哪个叙述是错误的？**
A. cat instanceof B的值是true。
B. bird instanceof A的值是true。
C. cat instanceof A的值是true。
D. bird instanceof C的值是true。

答：`D`
解：
D => 父类对象不是一种子类的类型

5. **下列程序中注释的哪个代码（A，B，C，D）是错误的（无法通过编译）？**

```java{.line-numbers}
class A {
   static int m;
   static void f(){
       m = 20 ;          //A
   }
}
class B extends A {
   void f()              //B
   {  m = 222 ;         //C
   }
}
class E {
   public static void main(String args[]) {
      A.f();            // D
   }
}
```

答：`B`
解：
B => `static`方法不能重写、只能重载（子类继承父类的`static`方法）

6. **下列代码中标注的（A,B,C,D）中，哪一个是错误的?**

```java{.line-numbers}
abstract class Takecare {
   protected void speakHello() {}     //A
   public abstract static void cry();    //B
   static int f(){ return 0 ;}          //C
   abstract float g();                //D
}
```

答：`B`
解：
A => 略
B => 不能同时用`abstract`和`static`修饰
C => 略
D => 略

7. **下列程序中注释的哪个代码（A，B，C，D）是错误的（无法通过编译）？**

```java{.line-numbers}
abstract class A {
   abstract float getFloat ();  //A
   void f()                //B
   { }
}
public class B extends A {
   private float m = 1.0f;      //C
   private float getFloat ()     //D
   {  return m;
   }
}
```

答：`D`
解：
A => 略
B => 略
C => 略
D => 重写方法不能降低访问权限的可见性（父类是`package access`，子类定义为`private access`）

8. **将下列哪个代码（A，B，C，D）放入程序中标注的【代码】处将导致编译错误？**
A. `public float getNum(){return 4.0f;}`
B. `public void getNum(){ }`
C. `public void getNum(double d){ }`
D. `public double getNum(float d){return 4.0d;}`

```java{.line-numbers}
class A {
   public float getNum() {
      return 3.0f;
   }
} 
public class B extends A { 
   //【代码】
}
```

答：`B`
解：
A => 重写方法
B => `void`不是`float`的子类型，因此错误重写
C => 形参列表不同，是重载
D => 形参列表不同，是重载

9. **对于下列代码，下列哪个叙述是正确的？**
A. 程序提示编译错误（原因是A类没有不带参数的构造方法）
B. 编译无错误，【代码】输出结果是0。
C. 编译无错误，【代码】输出结果是1。
D. 编译无错误，【代码】输出结果是2。

```java{.line-numbers}
class A { 
   public int i=0; 
   A(int m) {
     i = 1; 
   } 
} 
public class B extends A {
    B(int m) {
       i = 2; 
    } 
    public static void main(String args[]){ 
       B b = new B(100); 
       System.out.println(b.i); //【代码】
    } 
}
```

答：`A`
解：
A => 子类`B`的构造方法中需调用父类`A`的构造方法，由于`B(int m)`中没有显式调用`A(int m)`则默认调用`A`的无参构造方法，而`A`由于显式定义了一个有参构造方法，则不存在默认的无参构造方法

如果`A`类显式定义了无参构造方法或`B`类的构造方法显式调用了`A(int m)`（调用父类构造方法必须位于构造方法的首语句），则【代码】的输出结果是2

## 阅读程序

### 请说出E类中【代码1】，【代码2】的输出结果

```java{.line-numbers}
class A {
   double f(double x,double y) {
     return x+y;
   }
}
class B extends A {
   double f(int x,int y) {
     return x*y;
  }
}
public class E {
public static void main(String args[]) {
      B b=new B();
      System.out.println(b.f(3,5));     //【代码1】
      System.out.println(b.f(3.0,5.0));  //【代码2】
  }
}
```

答：
`【代码1】=> 15.0`
`【代码2】=> 8.0`
解：
`B`类重载了`A`类的`f(double x,double y)`方法
【代码1】 => 调用`B`类定义的`double f(int x,int y)`
【代码2】 => 调用`B`类继承的（`A`类定义）`double f(double x,double y)`

### 说出下列B类中【代码1】，【代码2】的输出结果

```java{.line-numbers}
class A {
   public int getNumber(int a){
      return a+1;
   }
}
class B extends A {
   public int getNumber (int a){
      return a+100;
   }
   public static void main (String args[]){
      A a =new A();
      System.out.println(a.getNumber(10));  //【代码1】
      a = new B();
      System.out.println(a.getNumber(10));  //【代码2】
   }
}
```

答：
`【代码1】 => 11`
`【代码2】 => 110`
解：
`B`类重写`A`类的`public int getNumber(int a)`方法，体现多态性
【代码1】 => 对象的类型是`A`，调用`A`类定义的方法
【代码2】=> 对象的类型是`B`，调用`B`类定义的方法

### 请说出E类中【代码1】~【代码4】的输出结果

```java{.line-numbers}
class A {
   double f(double x,double y) {
      return x+y;
   }
   static int g(int n) {
      return n*n;
   }
}
class B extends A {
   double f(double x,double y) {
      double m = super.f(x,y);
      return m+x*y;
   }
   static int g(int n) {
      int m = A.g(n);
      return m+n;
   }
}
public class E {
   public static void main(String args[]) {
     B b = new B();
     System.out.println(b.f(10.0,8.0));   //【代码1】
     System.out.println(b.g(3));        //【代码2】
     A a = new B();
     System.out.println(a.f(10.0,8.0));   //【代码3】
     System.out.println(a.g(3));        //【代码4】
  }
}
```

答：
`【代码1】 => 98.0`
`【代码2】 => 12`
`【代码3】 => 98.0`
`【代码4】 => 9`
解：
`B`类重写了`A`类的`double f(double x,double y)`方法，`B`类隐藏了`A`类的`static int g(int n)`方法

【代码1】 => 对象的类型是`B`，调用`B`类定义的`double f(double x,double y)`
【代码2】 => 引用变量`b`的类型是`B`，调用`B`类定义的`static int g(int n)`
【代码3】 => 对象的类型是`B`，调用`B`类定义的`double f(double x,double y)`
【代码4】 => 引用变量`a`的类型是`A`，调用`A`类定义的`static int g(int n)`

**:warning:静态方法没有多态性，即调用方法取决于引用变量的类型，而不是对象的类型**

### 请说出E类中【代码1】~【代码3】的输出结果

```java{.line-numbers}
class A {
   int m;
   int getM() {
      return m;
   }
   int seeM() {
       return m;
   }
}
class B extends A {
     int m ;
     int getM() {
        return m+100;
     }
}
public class E {
public static void main(String args[]) {
     B b = new B();
     b.m = 20;
     System.out.println(b.getM());  //【代码1】
     A a = b;
     a.m = -100;                 // 上转型对象访问的是被隐藏的m 
     System.out.println(a.getM());  //【代码2】上转型对象调用的一定是子类重写的getM()方法
     System.out.println(b.seeM()); //【代码3】子类继承的seeM()方法操作的m是被子类隐藏的m
  }
}
```

答：
`【代码1】 => 120`
`【代码2】 => 120`
`【代码3】 => -100`
解：
`B`类隐藏了`A`类的成员变量`m`，`B`类重写了`A`类的`getM()`方法

【代码1】 => 对象类型是`B`，调用`B`类定义的`getM()`方法
【代码2】 => 对象类型是`B`，调用`B`类定义的`getM()`方法，由于成员变量没有多态性，故`a.m = -100`访问的是`A`类定义的`m`而`B`类定义的`m`仍为`20`（即对象有两个名为`m`的成员变量）
【代码3】 => `B`类没有重写`int seeM()`方法，调用`A`类定义的`seeM()`方法，`seeM()`方法中访问的是`A`类定义的`m`（`A`类并不知道`B`类的存在，故不可能访问`B`类的`m`）

## 编程题（参考例子13）

设计一个动物声音“模拟器”，希望模拟器可以模拟许多动物的叫声。要求如下：

1. 编写抽象类Animal：Animal抽象类有2个抽象方法cry()和getAnimaName()，即要求各种具体的动物给出自己的叫声和种类名称。
1. 编写模拟器类Simulator：该类有一个playSound(Animal animal)方法，该方法的参数是Animal类型。即参数animal可以调用Animal的子类重写的cry()方法播放具体动物的声音、调用子类重写的getAnimalName()方法显示动物种类的名称。
1. 编写Animal类的子类：Dog，Cat类：图5.18是Simulator、Animal、Dog、Cat的UML图。
1. 编写主类Application（用户程序）：在主类Application的main方法中至少包含如下代码：
   + Simulator simulator = new Simulator();
   + simulator.playSound(new Dog());
   + simulator.playSound(new Cat());
