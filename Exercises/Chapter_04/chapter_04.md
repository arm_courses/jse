# Chapter04 Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编程题（参考例子7~9）](#编程题参考例子7~9)

<!-- /code_chunk_output -->

## 问答题

1. 面向对象语言有哪三个特性？ => 封装、继承、多态
2. 类名应当遵守怎样的编程风格？ => 能体现功能有意义的多个英文单词，每个单词首字母大写
3. 变量和方法的名字应当遵守怎样的编程风格？ => 能体现功能有意义的多个英文单词，首单词首字母小写、其余单词首字母大写
4. 类体内容中声明成员变量是为了体现对象的属性还是行为？ => 属性
5. 类体内容中定义的非构造方法是为了体现对象的属性还是行为？ => 行为
6. 什么时候使用构造方法？构造方法有类型吗？ => 创建对象时（更准确的描述是：分配完内存后初始化对象时），构造方法没有类型
7. 类中的实例变量在什么时候会被分配内存空间？ => 创建对象时
8. 什么叫方法的重载？构造方法可以重载吗？ => 同名不同形参列表的多个方法（个数不同或类型不同），构造方法可以重载
9. 类中的实例方法可以操作类变量（static变量）吗？类方法（static方法）可以操作实例变量吗？ => 实例方法可以操作类变量，类方法不可以操作实例变量（类方法独立于具体对象，实例变量在创建对象时分配内存空间，因此，调用类方法时，不知道调用哪个对象的内存空间也可以没有任何实例变量）
10. 类中的实例方法可以用类名直接调用吗？=> 不可以
11. 简述类变量和实例变量的区别。 => 类变量属于类（只有一份），实例变量属于对象（一个对象一份）
12. this关键字代表什么？this可以出现在类方法中吗？ => `this`代表当前方法所属对象，`this`不可以出现在类方法中（`this`是对象级，类方法是类级）

## 选择题

1. **下列哪个叙述是正确的？**
A. Java应用程序由若干个类所构成，这些类必须在一个源文件中
B. Java应用程序由若干个类所构成，这些类可以在一个源文件中，也可以分布在若干个源文件中，其中必须有一个源文件含有主类
C. Java源文件必须含有主类
D. Java源文件如果含有主类，主类必须是public类

答：`B`
解：
A => 见B选项
B => 略
C => 一个Java应用程序至少有一个主类即可
D => 主类不必一定是public类（但推荐主类是`public类`）

2. **下列哪个叙述是正确的？**
A. 成员变量的名字不可以和局部变量的相同。
B. 方法的参数的名字可以和方法中声明的局部变量的名字相同。
C. 成员变量没有默认值。
D. 局部变量没有默认值。

答：`D`
解：
A => 在方法的作用域内局部变量隐藏同名的成员变量
B => 方法的参数（形参）也是局部变量
C => 成员变量有默认值
D => 局部变量没默认值

3. **对于下列Hello类，哪个叙述是正确的？**
A. Hello类有2个构造方法。
B. Hello类的int Hello()方法是错误的方法。
C. Hello类没有构造方法。
D. Hello无法通过编译，因为其中的hello方法的方法头是错误的（没有类型）。

```java{.line-numbers}
class Hello {
   Hello(int m){
   }
   int Hello() {
      return 20;
   }
   hello() {
   }
}
```

答：`D`
解：
`Hello(int m)`是构造方法，`int Hello()`是成员方法（构造方法没有返回值）

A => 1个构造方法
B => 成员方法
C => 1个构造方法
D => 略

4. **对于下列Dog类，哪个叙述是错误的？**
A. Dog(int m)与Dog(double m)互为重载的构造方法。
B. int Dog(int m)与void Dog(double m)互为重载的非构造方法。
C. Dog类只有两个构造方法，而且没有无参数的构造方法。
D. Dog类有3个构造方法。

```java{.line-numbers}
class Dog {
   Dog(int m){
   }
   Dog(double  m){
   }
   int Dog(int m){
      return 23;
   }
   void Dog(double m){
   }
}
```

答：`D`
解：参见各叙述正确的选项

5. **下列哪些类声明是错误的？**
A. class A
B. public class A
C. protected class A
D. private class A

答：`CD`

解：`private`和`protected`不能修饰`class`

6. **下列A类中【代码1】~【代码5】哪些是错误的？**

```java{.line-numbers}
class Tom {
   private int x = 120;
   protected int y = 20;
   int z = 11;
   private void f() {
      x = 200;
      System.out.println(x);
   }
   void g() {
      x = 200;
      System.out.println(x);
   }
}
public class A {
  public static void main(String args[]) {
      Tom tom = new Tom();
      tom.x = 22;   //【代码1】
      tom.y = 33;   //【代码2】
      tom.z = 55;   //【代码3】
      tom.f();     //【代码4】
      tom.g();     //【代码5】
   }
}
```

答：`【代码1】、【代码4】`
解：
【代码1】 => `private int x`
【代码4】=> `private void f()`

7．**下列E类的类体中哪些【代码】是错误的。**

```java{.line-numbers}
class E {
   int x;               //【代码1】
   long y = x;          //【代码2】
   public void f(int n) {
      int m;          //【代码3】
      int t = n+m;     //【代码4】
   }
}
```

答：`【代码4】`
解：
【代码4】 => 局部变量`m`未初始化（局部变量没有默认值、成员变量有默认值）

## 阅读程序

### 说出下列E类中【代码1】~【代码3】的输出结果

```java{.line-numbers}
class Fish {
   int weight = 1;
}
class Lake {
   Fish fish;
   void setFish(Fish s){
       fish = s;
   }
   void foodFish(int m) {
      fish.weight = fish.weight + m;
   }
}
public class E {
   public static void main(String args[]) {
      Fish  redFish = new Fish();
      System.out.println(redFish.weight);  //【代码1】
      Lake lake = new Lake();
      lake.setFish(redFish);
      lake.foodFish(120);
      System.out.println(redFish.weight);  //【代码2】
      System.out.println(lake.fish.weight);  //【代码3】
   }
}
```

答：
`【代码1】 => 1`
`【代码2】 => 121`
`【代码3】 => 121`
解：
`lake`中引用变量`fish`引用的对象与引用变量`redFish`引用的对象是同一个对象，因此通过任何一个引用变量都是修改了同一个对象

### 请说出A类中System.out.println的输出结果

```java{.line-numbers}
class B {
   int x = 100,y = 200;
   public void setX(int x) {
      x = x;
   }
   public void setY(int y) {
      this.y = y;
   }
   public int getXYSum() {
      return x+y;
   }
}
public class A {
   public static void main(String args[]) {
      B b = new B();
      b.setX(-100);
      b.setY(-200);
      System.out.println("sum="+b.getXYSum());
   }
}
```

答：`sum=-100`

解：`B`类中的`public void setX(int x)`方法中`x = x`没有修改成员变量`x`的值，因此，程序执行完`x`的值仍为100，而`y`的值为`-200`

### 请说出A类中System.out.println的输出结果

```java{.line-numbers}
class B {
   int n;
   static int sum=0;
   void setN(int n) {
      this.n=n;
   }
   int getSum() {
      for(int i=1;i<=n;i++)
         sum=sum+i;
      return sum;
   }
}
public class A {
   public static void main(String args[]) {
      B b1=new B(),b2=new B();
      b1.setN(3);
      b2.setN(5);
      int s1=b1.getSum();
      int s2=b2.getSum();
      System.out.println(s1+s2);
   }
}
```

答：`27`

解：
`sum`是静态变量，因此两次`getSum()`为累加，即

1. s1 = 1 + 2 + 3 = 6
1. s2 = 6 + ( 1 + 2 + 3 + 4 + 5 )= 21
1. s1 + s2 = 6 + 21 = 27

### 请说出E类中【代码1】,【代码2】的输出结果n的输出结果

```java{.line-numbers}
class A {
   double f(int x,double y) {
      return x+y;
   }
   int f(int x,int y) {
      return x*y;
   }
}
public class E {
   public static void main(String args[]) {
      A a=new A();
      System.out.println(a.f(10,10)); //【代码1】
      System.out.println(a.f(10,10.0)); //【代码2】
  }
}
```

答：
`【代码1】 => 100`
`【代码2】 => 20.0`
解：
【代码1】 => 调用`int f(int x,int y)`
【代码2】 => 调用`double f(int x,double y)`

### 上机实习下列程序，了解可变参数

```java{.line-numbers}
public class E {
   public static void main(String args[]) {
         f(1,2);
         f(-1,-2,-3,-4); //给参数传值时，实参的个数很灵活
         f(9,7,6) ;
   }
   public static void f(int ... x){    //x是可变参数的代表，代表若干个int型参数
        for(int i=0;i<x.length;i++) {  //x.length是x代表的参数的个数
            System.out.println(x[i]);  //x[i]是x代表的第i个参数(类似数组)
        }
   }
}
```

答： 略

### 类的字节码进入内存时，类中的静态块会立刻被执行，执行下列程序，了解静态块

```java{.line-numbers}
class AAA {
   static { //静态块
      System.out.println("我是AAA中的静态块!");
   }
}
public class E {
   static { //静态块
      System.out.println("我是最先被执行的静态块!");
   }
   public static void main(String args[]) {
      AAA a= new AAA();  //AAA的字节码进入内存
      System.out.println("我在了解静态(static)块");  
   }
}
```

答： 略

## 编程题（参考例子7~9）

用类描述计算机中CPU的速度和硬盘的容量。要求Java应用程序有4个类，名字分别是PC，CPU和HardDisk和Test，其中Test是主类。

PC类与CPU和HardDisk类关联的UML图（图4.33）

其中，CPU类要求getSpeed()返回speed的值；要求setSpeed(int m)方法将参数m的值赋值给speed。HardDisk类要求getAmount()返回amount的值，要求setAmount(int m)方法将参数m的值赋值给amount。PC类要求setCUP(CPU c) 将参数c的值赋值给cpu，要求setHardDisk (HardDisk h)方法将参数h的值赋值给HD，要求show()方法能显示cpu的速度和硬盘的容量。

主类Test的要求

1. main方法中创建一个CPU对象cpu，cpu将自己的speed设置为2200
1. main方法中创建一个HardDisk对象disk，disk将自己的amount设置为200
1. main方法中创建一个PC对象pc
1. pc调用setCUP(CPU c)方法，调用时实参是cpu
1. pc调用setHardDisk (HardDisk h)方法，调用时实参是disk
1. pc调用show()方法
